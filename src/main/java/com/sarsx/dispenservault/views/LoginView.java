package com.sarsx.dispenservault.views;

import com.sarsx.dispenservault.services.RoleService;
import com.sarsx.dispenservault.services.UserService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Route(value = LoginView.ROUTE)
@PageTitle("Login")
@Component
@UIScope
public class LoginView extends HorizontalLayout {
    static final String ROUTE = "login";

    private UserService userService;
    private RoleService roleService;

    private LoginForm login = new LoginForm();
    private Button registration;
    private LoginI18n i18n = LoginI18n.createDefault();
    private VerticalLayout layout = new VerticalLayout();
    private RegistrationForm registrationForm;
    private Dialog dialog;

    @Autowired
    public LoginView(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
        registrationForm = new RegistrationForm(userService, roleService);
        dialog = new Dialog();
        registration = new Button("Registration");
        login.setAction("login");
        login.setForgotPasswordButtonVisible(false);
        getElement().appendChild(login.getElement());
        login.setI18n(i18n);
        layout.add(login, registration);
        layout.setAlignItems(Alignment.CENTER);
        layout.setSizeUndefined();
        add(layout);
        setSizeFull();
        setAlignItems(Alignment.CENTER);
        setJustifyContentMode(JustifyContentMode.CENTER);

        Button save = new Button("Save");
        save.setWidth("100%");
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        save.addClickListener(event -> {
            userService.save(registrationForm.getUserBean());
            new Notification("You have successfully registered", 3000);
            dialog.close();
        });

        Button cancel = new Button("Cancel");
        cancel.setWidth("100%");
        cancel.addClickListener(event -> {
            dialog.close();
        });

        dialog.add(registrationForm, save, cancel);

        registration.addClickListener(event -> dialog.open());
    }
}