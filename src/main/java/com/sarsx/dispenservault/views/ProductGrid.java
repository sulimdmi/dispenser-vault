package com.sarsx.dispenservault.views;

import com.sarsx.dispenservault.models.Product;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.stereotype.Component;

@Component
@UIScope
public class ProductGrid extends Grid<Product> {
    public ProductGrid() {
        setSizeFull();

        addColumn(Product::getBarcode).setHeader("Barcode")
                .setFlexGrow(3).setSortable(true).setKey("barcode");

        addColumn(Product::getName).setHeader("Name")
                .setFlexGrow(5).setSortable(true).setKey("name");

        addColumn(Product::getProductManufacturer).setHeader("Manufacturer")
                .setFlexGrow(5).setSortable(true).setKey("manufacturer");

        addColumn(Product::getType).setHeader("Type")
                .setFlexGrow(3).setSortable(true).setKey("type");

        addColumn(Product::getQuantity).setHeader("Quantity")
                .setFlexGrow(2).setSortable(true).setKey("quantity");

        addColumn(Product::getPrice).setHeader("Price").setFlexGrow(2).setSortable(true).setKey("price");
    }

    private void setColumnVisibility(int width) {
        if (width > 800) {
            getColumnByKey("barcode").setVisible(true);
            getColumnByKey("name").setVisible(true);
            getColumnByKey("manufacturer").setVisible(true);
            getColumnByKey("type").setVisible(true);
            getColumnByKey("quantity").setVisible(true);
            getColumnByKey("price").setVisible(true);
        } else if (width > 550) {
            getColumnByKey("barcode").setVisible(true);
            getColumnByKey("name").setVisible(true);
            getColumnByKey("manufacturer").setVisible(true);
            getColumnByKey("type").setVisible(false);
            getColumnByKey("quantity").setVisible(true);
            getColumnByKey("price").setVisible(true);
        } else {
            getColumnByKey("barcode").setVisible(false);
            getColumnByKey("name").setVisible(true);
            getColumnByKey("manufacturer").setVisible(true);
            getColumnByKey("type").setVisible(false);
            getColumnByKey("quantity").setVisible(true);
            getColumnByKey("price").setVisible(true);
        }
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);

        UI.getCurrent().getInternals().setExtendedClientDetails(null);
        UI.getCurrent().getPage().retrieveExtendedClientDetails(e -> setColumnVisibility(e.getBodyClientWidth()));
    }
}
