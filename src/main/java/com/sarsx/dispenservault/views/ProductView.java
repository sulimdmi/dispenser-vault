package com.sarsx.dispenservault.views;

import com.sarsx.dispenservault.models.*;
import com.sarsx.dispenservault.services.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Route(value = "products", layout = MainLayout.class)
@UIScope
public class ProductView extends HorizontalLayout {
    public static final String VIEW_NAME = "Products";
    private final ProductGrid grid;
    private TextField filter;

    private Button newProduct;

    private Product productFromDb;

    private Dialog dialog;

    private Select<ProductManufacturer> manufacturer;
    private Select<Product> name;
    private TextField quantity;

    private Button save;
    private Button cancel;
    private Button delete;

    private Select<Manufacturer> manufacturerSelector;
    private Select<Model> modelSelector;

    private VerticalLayout content;

    private ModelService modelService;
    private ProductService productService;
    private DispenserService dispenserService;
    private ManufacturerService manufacturerService;
    private LocationService locationService;
    private ProductManufacturerService productManufacturerService;

    private Button button;

    private Select<Location> locationSelector;

    @Autowired
    public ProductView(ProductService productService,
                       DispenserService dispenserService,
                       ManufacturerService manufacturerService,
                       ModelService modelService,
                       LocationService locationService,
                       ProductManufacturerService productManufacturerService) {
        this.productService = productService;
        this.dispenserService = dispenserService;
        this.manufacturerService = manufacturerService;
        this.modelService = modelService;
        this.locationService = locationService;
        this.productManufacturerService = productManufacturerService;

        setSizeFull();

        final HorizontalLayout topLayout = createTopBar();

        grid = new ProductGrid();

        dialog = new Dialog();
        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);

        grid.asSingleSelect().addValueChangeListener(event -> setProductFromDb(event.getValue()));
        grid.asSingleSelect().addValueChangeListener(event -> onSelectDispenserGridRow());

        final VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.add(topLayout);
        barAndGridLayout.add(grid);
        barAndGridLayout.setFlexGrow(1, grid);
        barAndGridLayout.setFlexGrow(0, topLayout);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.expand(grid);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);

        add(barAndGridLayout);

        content = new VerticalLayout();
        content.setSizeUndefined();

        manufacturer = new Select<>();
        manufacturer.setLabel("Manufacturer");
        manufacturer.setWidth("100%");
        manufacturer.setItems(productManufacturerService.findAll());
        manufacturer.addValueChangeListener(event -> {
            name.setItems(productService.findAllByProductManufacturer(manufacturer.getValue()));
            name.setEnabled(true);
        });

        name = new Select<>();
        name.setLabel("Name");
        name.setWidth("100%");
        name.setEnabled(false);

        HorizontalLayout horizontalLayout = new HorizontalLayout(manufacturer, name);
        horizontalLayout.setWidth("100%");
        horizontalLayout.setFlexGrow(1, manufacturer, name);
        content.add(horizontalLayout);

        quantity = new TextField();
        quantity.setLabel("Quantity");
        quantity.setWidth("100%");
        quantity.setValue("1");
        content.add(quantity);

        save = new Button("Save");
        save.setWidth("100%");
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        save.addClickListener(event -> {
            Dispenser currentDispenser = dispenserService.findByModelAndUser(modelSelector.getValue(), (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
            productService.save(getProduct(currentDispenser));
            loadProducts();
            dialog.close();
            new Notification("Product successfully added", 3000).open();
        });

        cancel = new Button("Cancel");
        cancel.setWidth("100%");
        cancel.addClickListener(event -> dialog.close());

        delete = new Button("Delete");
        delete.setWidth("100%");
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_PRIMARY);
        delete.addClickListener(event -> {
            //TODO implement delete operation
        });

        Div f = new Div();
        f.add(content);
        dialog.add(f);

        Set<Manufacturer> man = new HashSet<>();
        getDispensersByUser().forEach(item -> man.add(item.getManufacturer()));

        manufacturerSelector.setItems(man);
        manufacturerSelector.addValueChangeListener(event -> {
            Set<Model> mod = new HashSet<>();

            for (Dispenser dispenser : getDispensersByUser()) {
                if (dispenser.getManufacturer().equals(manufacturerSelector.getValue())) {
                    mod.add(dispenser.getModel());
                }
            }

            modelSelector.setEnabled(true);
            modelSelector.setItems(mod);
        });

        modelSelector.addValueChangeListener(event -> {
            loadProducts();
        });
        //TODO add location search

        button.addClickListener(click -> {

        });

        content.add(save, delete, cancel);
    }

    private void loadProducts() {
        Dispenser currentDispenser = dispenserService.findByModelAndUser(modelSelector.getValue(), (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        List<Product> currentProducts = productService.findAllByDispenser(currentDispenser);
        grid.setItems(currentProducts);
    }

    private Product getProduct(Dispenser dispenser) {
        Product product  = new Product();

        product.setBarcode(name.getValue().getBarcode());
        product.setProductManufacturer(manufacturer.getValue());
        product.setName(name.getValue().getName());
        product.setType(name.getValue().getType());
        product.setQuantity(Integer.parseInt(quantity.getValue()));
        product.setDispenser(dispenser);

        return product;
    }

    public List<Dispenser> getDispensersByUser() {
        return dispenserService.findAllByUser((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    private void onAddDispenserButtonClick() {
        dialog.open();
        delete.setVisible(false);
        save.setText("Add");
    }

    private void onSelectDispenserGridRow() {
        dialog.open();
        delete.setVisible(true);
        save.setText("Save");
    }

    public HorizontalLayout createTopBar() {
        manufacturerSelector = new Select<>();
        manufacturerSelector.setPlaceholder("Select manufacturer");

        modelSelector = new Select<>();
        modelSelector.setPlaceholder("Select model");
        modelSelector.setEnabled(false);

        locationSelector = new Select<>();
        locationSelector.setPlaceholder("Select identifier");
        locationSelector.setEnabled(false);

        button = new Button("load");

        filter = new TextField();
        filter.setPlaceholder("Filter manufacturer, model or type");
        //TODO implement filter by product barcode, manufacturer or product name

        newProduct = new Button("Add product");
        newProduct.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        newProduct.setIcon(VaadinIcon.PLUS_CIRCLE.create());
        newProduct.addClickListener(click -> onAddDispenserButtonClick());

        final HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setWidth("100%");
        topLayout.add(manufacturerSelector);
        topLayout.add(modelSelector);
        topLayout.add(locationSelector);
        topLayout.add(filter);
        topLayout.add(newProduct);
        topLayout.setVerticalComponentAlignment(Alignment.START, filter);
        topLayout.expand(filter);
        return topLayout;
    }

    public Product getProductFromDb() {
        return productFromDb;
    }

    private void setProductFromDb(Product productFromDb) {
        this.productFromDb = productFromDb;
    }
}
