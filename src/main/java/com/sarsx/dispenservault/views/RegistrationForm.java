package com.sarsx.dispenservault.views;

import com.sarsx.dispenservault.models.Role;
import com.sarsx.dispenservault.models.User;
import com.sarsx.dispenservault.services.RoleService;
import com.sarsx.dispenservault.services.UserService;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
@UIScope
@Route("registration")
public class RegistrationForm extends FormLayout {
    private TextField username;
    private TextField password;
    private TextField email;
    private Checkbox customer;
    private Checkbox dispenserProvider;
    private Checkbox productManager;

    private Binder<User> binder;

    private UserService userService;
    private RoleService roleService;

    @Autowired
    public RegistrationForm(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;

        setSizeUndefined();

        VerticalLayout content = new VerticalLayout();
        content.setSizeUndefined();

        add(content);

        username = new TextField("User name");
        username.setWidth("100%");
        username.setRequired(true);
        username.setValueChangeMode(ValueChangeMode.EAGER);
        content.add(username);

        password = new TextField("Password");
        password.setWidth("100%");
        password.setRequired(true);
        password.setValueChangeMode(ValueChangeMode.EAGER);
        content.add(password);

        email = new TextField("Email");
        email.setWidth("100%");
        email.setRequired(true);
        email.setValueChangeMode(ValueChangeMode.EAGER);
        content.add(email);

        customer = new Checkbox();
        customer.setLabel("Simple user");
        dispenserProvider = new Checkbox();
        dispenserProvider.setLabel("Dispenser provider");
        productManager = new Checkbox();
        productManager.setLabel("Product manager");

        HorizontalLayout checkboxes = new HorizontalLayout();
        checkboxes.add(customer, dispenserProvider, productManager);
        content.add(checkboxes);

        binder = new BeanValidationBinder<>(User.class);
        binder.forField(username)
                .withNullRepresentation(" ")
                .bind(User::getUsername, User::setUsername);
        binder.forField(password)
                .withNullRepresentation(" ")
                .bind(User::getPassword, User::setPassword);
        binder.forField(email)
                .withNullRepresentation(" ")
                .bind(User::getEmail, User::setEmail);
    }

    User getUserBean() {
        User user = new User();
        Set<Role> roles = new HashSet<>();

        if (customer.getValue().equals(true)) {
            roles.add(roleService.findByRole("customer"));
        }
        if (dispenserProvider.getValue().equals(true)) {
            roles.add(roleService.findByRole("dispenser_provider"));
        }
        if (productManager.getValue().equals(true)) {
            roles.add(roleService.findByRole("product_manager"));
        }

        if (binder.writeBeanIfValid(user)) {
            user.setRoles(roles);
            return user;
        }

        return null;
    }
}
