package com.sarsx.dispenservault.views;

import com.sarsx.dispenservault.models.*;
import com.sarsx.dispenservault.services.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Route(value = "vault", layout = MainLayout.class)
@UIScope
public class DispenserView extends HorizontalLayout {
    public static final String VIEW_NAME = "Dispensers";

    private final DispenserGrid grid;
    private TextField filter;

    private ManufacturerService manufacturerService;
    private ModelService modelService;

    private Button addNew;

    private Dialog dialog;
    private Dialog editProductsDialog;

    private Select<Manufacturer> manufacturer;
    private Select<Model> model;
    private TextField location;
    private Button cancel;
    private Button delete;
    private Button edit;
    private Button add;
    private Button update;

    private Dispenser currentDispenser;
    private VerticalLayout content;
    private VerticalLayout productFormLayout;

    private ProductService productService;
    private LocationService locationService;
    private DispenserService dispenserService;
    private ProductManufacturerService productManufacturerService;

    private Grid<Product> productTable;
    private Select<ProductManufacturer> productManufacturerSelect;
    private Select<Product> productSelector;
    private Button addProduct;

    private List<Product> productList;

    private Button confirmButton;

    @Autowired
    public DispenserView(DispenserService dispenserService,
                         ProductService productService,
                         ManufacturerService manufacturerService,
                         ModelService modelService,
                         LocationService locationService,
                         ProductManufacturerService productManufacturerService) {
        this.dispenserService = dispenserService;
        this.productService = productService;
        this.manufacturerService = manufacturerService;
        this.modelService = modelService;
        this.locationService = locationService;
        this.productManufacturerService = productManufacturerService;

        setSizeFull();

        final HorizontalLayout topLayout = createTopBar();

        grid = new DispenserGrid();

        dialog = new Dialog();
        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);

        final VerticalLayout barAndGridLayout = new VerticalLayout();
        barAndGridLayout.add(topLayout);
        barAndGridLayout.add(grid);
        barAndGridLayout.setFlexGrow(1, grid);
        barAndGridLayout.setFlexGrow(0, topLayout);
        barAndGridLayout.setSizeFull();
        barAndGridLayout.expand(grid);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);

        add(barAndGridLayout);

        productList = new ArrayList<>();

        productManufacturerSelect = new Select<>();
        productManufacturerSelect.setWidth("100%");
        productManufacturerSelect.setPlaceholder("Select manufacturer");
        productManufacturerSelect.setItems(productManufacturerService.findAll());
        productManufacturerSelect.addValueChangeListener(event -> {
            productSelector.setItems(productService.findAllByProductManufacturer(event.getValue()));
            productSelector.setEnabled(true);
        });

        productSelector = new Select<>();
        productSelector.setEnabled(false);
        productSelector.setWidth("100%");
        productSelector.setPlaceholder("Select product");
        productSelector.setItems(productService.findAll());
        productSelector.addValueChangeListener(event -> addProduct.setEnabled(true));

        addProduct = new Button();
        addProduct.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        addProduct.setIcon(VaadinIcon.PLUS_CIRCLE.create());
        addProduct.setEnabled(false);
        addProduct.addClickListener(click -> {
            Product currentProduct = productSelector.getValue();
            currentProduct.setQuantity(0);
            productList.clear();
            productList.add(currentProduct); //adding to list of products
            productList.addAll(productTable.getDataProvider().fetch(new Query<>()).collect(Collectors.toList()));
            productTable.setItems(productList); //update product grid every time if product added
            productService.saveToDispenser(currentProduct, currentDispenser);
        });

        HorizontalLayout topEditProductLayout = new HorizontalLayout();
        topEditProductLayout.add(productManufacturerSelect, productSelector, addProduct);

        productTable = new Grid<>();
        productTable.addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_NO_ROW_BORDERS);
        productTable.addColumn(Product::getName).setHeader("Name").setFlexGrow(5).setKey("product_name");

        productTable.addComponentColumn(product -> {
            NumberField productCounter = new NumberField();
            productCounter.setMin(0d);
            productCounter.setMax(currentDispenser.getModel().getQueueSize());
            productCounter.setHasControls(true);
            productCounter.setValue((double) product.getQuantity());
            productCounter.addValueChangeListener(event -> {
                Product p = product;
                p.setQuantity(event.getValue().intValue());
                productService.update(product, p);
            });

            return productCounter;
        }).setHeader("Quantity").setFlexGrow(14).setKey("product_quantity");
        productTable.addComponentColumn(item -> {
            Icon deleteIcon = VaadinIcon.TRASH.create();
            deleteIcon.setSize("16px");
            Button deleteButton = new Button(deleteIcon);
            deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

            deleteButton.addClickListener(event -> {
                productService.delete(item);
                productTable.setItems(productService.findAllByDispenser(currentDispenser));
                new Notification("Product " + item.getName() + " successfully deleted", 3000).open();
                init();
            });

            return deleteButton;
        }).setFlexGrow(1);

        editProductsDialog = new Dialog();
        editProductsDialog.setCloseOnEsc(false);
        editProductsDialog.setCloseOnOutsideClick(false);

        confirmButton = new Button("Confirm");
        confirmButton.setWidth("100%");
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        confirmButton.addClickListener(event -> {
            dispenserService.update(currentDispenser, currentDispenser.checkStatus(productService.findAllByDispenser(currentDispenser)));
            init();
            editProductsDialog.close();
            //TODO add update or save operation for products
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setWidth("100%");
        cancelButton.addClickListener(event -> editProductsDialog.close());

        productFormLayout = new VerticalLayout();
        productFormLayout.setSizeUndefined();
        productFormLayout.add(topEditProductLayout, productTable, confirmButton, cancelButton);

        editProductsDialog.add(productFormLayout);

        content = new VerticalLayout();
        content.setSizeUndefined();

        manufacturer = new Select<>();
        manufacturer.setLabel("Manufacturer");
        manufacturer.setWidth("100%");
        manufacturer.setItems(this.manufacturerService.findAll());

        manufacturer.addValueChangeListener(event -> {
            model.setEnabled(true);
            model.setItems(this.modelService.findAllByManufacturer(manufacturer.getValue()));
        });

        model = new Select<>();
        model.setEnabled(false);
        model.setLabel("Model");
        model.setWidth("100%");

        HorizontalLayout horizontalLayout = new HorizontalLayout(manufacturer, model);
        horizontalLayout.setWidth("100%");
        horizontalLayout.setFlexGrow(1, manufacturer, model);
        content.add(horizontalLayout);

        location = new TextField("Location");
        location.setWidth("100%");
        location.setValueChangeMode(ValueChangeMode.EAGER);
        content.add(location);

        add = new Button("Add");
        add.setWidth("100%");
        add.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        update = new Button("Update");
        update.setWidth("100%");
        update.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        cancel = new Button("Cancel");
        cancel.setWidth("100%");
        cancel.addClickListener(event -> clearSelection());

        delete = new Button("Delete");
        delete.setWidth("100%");
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_PRIMARY);
        delete.addClickListener(event -> {
            init();
            clearSelection();
            dialog.close();
        });

        Div f = new Div();
        f.add(content);
        dialog.add(f);

        edit = new Button("Edit products");
        edit.setWidth("100%");
        edit.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS);

        Anchor addNewModel = new Anchor();
        addNewModel.setText("Add new dispenser");
        addNewModel.getElement().addEventListener("click", e -> Notification.show("hello"));

        update.addClickListener(click -> {
            Dispenser source = currentDispenser;

            source.getModel().setManufacturer(manufacturer.getValue());
            source.setModel(model.getValue());
            source.setLocation(locationService.save(new Location(location.getValue())));

            dispenserService.update(currentDispenser, source);

            new Notification("Dispenser successfully updated", 3000).open();

            dialog.close();
            init();
        });

        grid.addComponentColumn(dispenser -> {
            Icon editIcon = VaadinIcon.PENCIL.create();
            editIcon.setSize("16px");
            Button editButton = new Button(editIcon);

            editButton.addClickListener(event -> {
                dialog.open();
                add.setVisible(false);
                update.setVisible(true);
                delete.setVisible(true);
                edit.setVisible(true);
                currentDispenser = dispenser;

                manufacturer.setValue(dispenser.getManufacturer());
                model.setValue(dispenser.getModel());
                location.setValue(dispenser.getLocation().getAddress());
            });

            return editButton;
        });

        edit.addClickListener(event -> {
            editProductsDialog.open();
            productTable.setItems(productService.findAllByDispenser(currentDispenser));
        });

        grid.addComponentColumn(dispenser -> {
            Icon deleteIcon = VaadinIcon.TRASH.create();
            deleteIcon.setSize("16px");
            Button deleteButton = new Button(deleteIcon);
            deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

            deleteButton.addClickListener(event -> {
                dispenserService.delete(dispenser);
                new Notification("Dispenser successfully deleted", 3000).open();
                init();
            });

            return deleteButton;
        });

        addNew.addClickListener(click -> {
            dialog.open();
            update.setVisible(false);
            delete.setVisible(false);
            edit.setVisible(false);
            add.setVisible(true);
        });

        add.addClickListener(event -> {
            dispenserService.save(getDispenser());
            init();
            new Notification("Dispenser added successfully", 3000).open();
            dialog.close();
        });

        content.add(add, update, delete, edit, cancel, addNewModel);
    }

    @PostConstruct
    public void init() {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        grid.setItems(dispenserService.findAllByUser(currentUser));
    }

    private Product getProduct(Dispenser dispenser) {
        Product product = new Product();

        product.setBarcode(productSelector.getValue().getBarcode());
        product.setProductManufacturer(productManufacturerSelect.getValue());
        product.setName(productSelector.getValue().getName());
        product.setType(productSelector.getValue().getType());
        product.setDispenser(dispenser);

        return product;
    }

    private Dispenser getDispenser() {
        Dispenser dispenser = new Dispenser();

        dispenser.setModel(model.getValue());
        dispenser.setUser((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        Location currentLocation = new Location(location.getValue());
        locationService.save(currentLocation);
        dispenser.setLocation(currentLocation);

        return dispenser;
    }

    private HorizontalLayout createTopBar() {
        filter = new TextField();
        filter.setPlaceholder("Filter manufacturer, name or type");
        filter.setClearButtonVisible(true);
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(event -> {
            if (event.getValue().length() == 0) {
                //TODO implement searching by manufacturer, type
                init();
            } else {
                grid.setItems(dispenserService.findAllByUserAndModel_Name((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal(), event.getValue()));
            }
        });

        addNew = new Button("Add dispenser");
        addNew.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        addNew.setIcon(VaadinIcon.PLUS_CIRCLE.create());

        final HorizontalLayout topLayout = new HorizontalLayout();
        topLayout.setWidth("100%");
        topLayout.add(filter);
        topLayout.add(addNew);
        topLayout.setVerticalComponentAlignment(Alignment.START, filter);
        topLayout.expand(filter);
        return topLayout;
    }

    private void clearSelection() {
        manufacturer.clear();
        model.clear();
        location.clear();
        dialog.close();
    }
}
