package com.sarsx.dispenservault.views;

import com.sarsx.dispenservault.models.Dispenser;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
@UIScope
public class DispenserGrid extends Grid<Dispenser> {
    public DispenserGrid() {

        setSizeFull();

        final String availabilityTemplate = "<iron-icon icon=\"vaadin:circle\" class-name=\"[[item.status]]\"></iron-icon> [[item.status]]";
        addColumn(TemplateRenderer.<Dispenser>of(availabilityTemplate)
                .withProperty("status",
                        dispenser -> dispenser.getStatus().toString()))
                .setHeader("Status")
                .setComparator(Comparator
                        .comparing(Dispenser::getStatus))
                .setFlexGrow(1).setKey("status");

        addColumn(Dispenser::getManufacturer).setHeader("Manufacturer")
                .setFlexGrow(5).setSortable(true).setKey("manufacturer");

        addColumn(Dispenser::getModel).setHeader("Model")
                .setFlexGrow(7).setSortable(true).setKey("model");

        addColumn(Dispenser::getLocation).setHeader("Location")
                .setFlexGrow(7).setSortable(true).setKey("location");
    }

    private void setColumnVisibility(int width) {
        if (width > 800) {
            getColumnByKey("status").setVisible(true);
            getColumnByKey("manufacturer").setVisible(true);
            getColumnByKey("model").setVisible(true);
            getColumnByKey("location").setVisible(true);
        } else if (width > 550) {
            getColumnByKey("status").setVisible(true);
            getColumnByKey("manufacturer").setVisible(true);
            getColumnByKey("model").setVisible(true);
            getColumnByKey("location").setVisible(true);
        } else {
            getColumnByKey("status").setVisible(true);
            getColumnByKey("manufacturer").setVisible(true);
            getColumnByKey("model").setVisible(true);
            getColumnByKey("location").setVisible(false);
        }
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);

        UI.getCurrent().getInternals().setExtendedClientDetails(null);
        UI.getCurrent().getPage().retrieveExtendedClientDetails(e -> setColumnVisibility(e.getBodyClientWidth()));
    }
}
