package com.sarsx.dispenservault.controllers;

import com.sarsx.dispenservault.models.Manufacturer;
import com.sarsx.dispenservault.services.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/dispenser-manufacturers")
public class ManufacturerController {
    private ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping
    public List<Manufacturer> findAll() {
        return manufacturerService.findAll();
    }

    @GetMapping("/{name}")
    public Manufacturer findByName(@PathVariable String name) {
        return manufacturerService.findByName(name);
    }
}
