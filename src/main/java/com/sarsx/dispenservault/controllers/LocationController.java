package com.sarsx.dispenservault.controllers;

import com.sarsx.dispenservault.models.Location;
import com.sarsx.dispenservault.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/locations")
public class LocationController {
    private LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @PostMapping
    public Location save(@Valid @RequestBody Location location) {
        return locationService.save(location);
    }

    @GetMapping
    public List<Location> findAll() {
        return locationService.findAll();
    }
}
