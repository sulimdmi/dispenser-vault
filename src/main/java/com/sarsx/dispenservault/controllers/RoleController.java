package com.sarsx.dispenservault.controllers;

import com.sarsx.dispenservault.models.Role;
import com.sarsx.dispenservault.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/roles")
public class RoleController {
    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping("/{role}")
    public Role findByRole(@PathVariable String role) {
        return roleService.findByRole(role);
    }
}
