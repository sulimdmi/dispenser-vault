package com.sarsx.dispenservault.controllers;

import com.sarsx.dispenservault.models.ProductManufacturer;
import com.sarsx.dispenservault.services.ProductManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/product-manufacturers")
public class ProductManufacturerController {
    private ProductManufacturerService productManufacturerService;

    @Autowired
    public ProductManufacturerController(ProductManufacturerService productManufacturerService) {
        this.productManufacturerService = productManufacturerService;
    }

    @GetMapping
    public List<ProductManufacturer> findAll() {
        return productManufacturerService.findAll();
    }
}
