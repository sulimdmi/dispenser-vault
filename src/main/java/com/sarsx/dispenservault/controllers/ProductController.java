package com.sarsx.dispenservault.controllers;

import com.sarsx.dispenservault.models.Dispenser;
import com.sarsx.dispenservault.models.Product;
import com.sarsx.dispenservault.models.ProductManufacturer;
import com.sarsx.dispenservault.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> findAll() {
        return productService.findAll();
    }

    @GetMapping("/dispensers/{dispenser}")
    public List<Product> findAllByDispenser(@PathVariable Dispenser dispenser) {
        return productService.findAllByDispenser(dispenser);
    }

    @GetMapping("/product-manufacturers/{productManufacturer}")
    public List<Product> findAllByProductManufacturer(@PathVariable ProductManufacturer productManufacturer) {
        return productService.findAllByProductManufacturer(productManufacturer);
    }

    @PostMapping("/list")
    public Product save(Product product) {
        return productService.save(product);
    }

    @PutMapping("/list/{id}")
    public ResponseEntity<Product> update(@PathVariable(value = "id") Long id,
                                                     @Valid @RequestBody Product product) {
        Product productUpdate = productService.getOne(id);

        if (product == null) {
            return ResponseEntity.notFound().build();
        }

        productUpdate.setBarcode(product.getBarcode());
        productUpdate.setProductManufacturer(product.getProductManufacturer());
        productUpdate.setName(product.getName());
        productUpdate.setType(product.getType());
        productUpdate.setQuantity(product.getQuantity());
        productUpdate.setDispenser(product.getDispenser());

        Product productSave = productService.save(productUpdate);

        return ResponseEntity.ok().body(productSave);
    }

    @DeleteMapping
    public void delete(Product product) {
        productService.delete(product);
    }
}
