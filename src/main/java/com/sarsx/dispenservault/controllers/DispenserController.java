package com.sarsx.dispenservault.controllers;

import com.sarsx.dispenservault.models.Dispenser;
import com.sarsx.dispenservault.models.Model;
import com.sarsx.dispenservault.models.User;
import com.sarsx.dispenservault.services.DispenserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/dispensers")
public class DispenserController {
    private DispenserService dispenserService;

    @Autowired
    public DispenserController(DispenserService dispenserService) {
        this.dispenserService = dispenserService;
    }

    @PostMapping
    public Dispenser save(@Valid @RequestBody Dispenser dispenser,
                                  @AuthenticationPrincipal User user) {
        dispenser.setUser(user);
        return dispenserService.save(dispenser);
    }

    //TODO add admin view support to activate this feature
    /*@GetMapping("all")
    public List<Dispenser> findAll() {
        return dispenserService.findAll();
    }*/

    @GetMapping("/id/{id}")
    public ResponseEntity<Dispenser> getOne(@PathVariable(value = "id") Long id) {
        Dispenser dispenser = dispenserService.getOne(id);

        if (dispenser == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(dispenser);
    }

    @GetMapping("/users/{user}")
    public List<Dispenser> findAllByUser(@PathVariable User user) {
        return dispenserService.findAllByUser(user);
    }

    @GetMapping("{model}/{user}")
    public Dispenser findByModelAndUser(@PathVariable Model model, @PathVariable User user) {
        return dispenserService.findByModelAndUser(model, user);
    }

    @GetMapping("/{user}/{model}")
    public List<Dispenser> findAllByUserAndModel_Name(@PathVariable User user, @PathVariable String model) {
        return dispenserService.findAllByUserAndModel_Name(user, model);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Dispenser> updateDispenser(@PathVariable(value = "id") Long id,
                                                     @Valid @RequestBody Dispenser dispenser) {
        Dispenser dispenserUpdate = dispenserService.getOne(id);

        if (dispenser == null) {
            return ResponseEntity.notFound().build();
        }

        dispenserUpdate.setModel(dispenser.getModel());
        dispenserUpdate.setLocation(dispenser.getLocation());
        dispenserUpdate.setUser(dispenser.getUser());
        dispenserUpdate.setStatus(dispenser.getStatus());

        Dispenser dispenserSave = dispenserService.save(dispenserUpdate);

        return ResponseEntity.ok().body(dispenserSave);
    }

    @DeleteMapping
    public void deleteDispenser(Dispenser dispenser) {
        dispenserService.delete(dispenser);
    }
}
