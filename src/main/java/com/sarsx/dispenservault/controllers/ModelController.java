package com.sarsx.dispenservault.controllers;

import com.sarsx.dispenservault.models.Manufacturer;
import com.sarsx.dispenservault.models.Model;
import com.sarsx.dispenservault.services.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/dispenser-models")
public class ModelController {
    private ModelService modelService;

    @Autowired
    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    @GetMapping("/models/all/{manufacturer}")
    public List<Model> findAllModelsByManufacturer(@PathVariable Manufacturer manufacturer) {
        return modelService.findAllByManufacturer(manufacturer);
    }

    @PostMapping("/models")
    public Model save(@Valid @RequestBody Model model) {
        return modelService.save(model);
    }

    @DeleteMapping("/models/{model}")
    public void delete(@PathVariable Model model) {
        modelService.delete(model);
    }
}
