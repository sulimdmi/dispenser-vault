package com.sarsx.dispenservault.services;

import com.sarsx.dispenservault.models.Manufacturer;
import com.sarsx.dispenservault.models.Model;
import com.sarsx.dispenservault.repositories.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelService {
    private ModelRepository modelRepository;

    @Autowired
    public ModelService(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    public List<Model> findAll() {
        return modelRepository.findAll();
    }

    public List<Model> findAllByManufacturer(Manufacturer manufacturer) {
        return modelRepository.findAllByManufacturer(manufacturer);
    }

    public Model save(Model model) {
        return modelRepository.save(model);
    }

    public void delete(Model model) {
        modelRepository.delete(model);
    }
}
