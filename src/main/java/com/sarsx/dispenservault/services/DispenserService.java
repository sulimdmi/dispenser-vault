package com.sarsx.dispenservault.services;

import com.sarsx.dispenservault.models.Dispenser;
import com.sarsx.dispenservault.models.Model;
import com.sarsx.dispenservault.models.User;
import com.sarsx.dispenservault.repositories.DispenserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DispenserService {
    private DispenserRepository dispenserRepository;

    @Autowired
    public DispenserService(DispenserRepository dispenserRepository) {
        this.dispenserRepository = dispenserRepository;
    }

    public Dispenser save(Dispenser dispenser) {
        return dispenserRepository.save(dispenser);
    }

    //TODO add admin view support to activate this feature
    /*public List<Dispenser> findAll() {
        return dispenserRepository.findAll();
    }*/

    public List<Dispenser> findAllByUserAndModel_Name(User user, String name) {
        return dispenserRepository.findAllByUserAndModel_Name(user, name);
    }

    public List<Dispenser> findAllByUser(User user) {
        return dispenserRepository.findAllByUser(user);
    }

    public Dispenser findByModelAndUser(Model model, User user) {
        return dispenserRepository.findByModelAndUser(model, user);
    }

    public Dispenser getOne(Long id) {
        return dispenserRepository.getOne(id);
    }

    public void update(Dispenser target, Dispenser source) {
        target.setModel(source.getModel());
        target.setLocation(source.getLocation());
        target.setUser(source.getUser());
        target.setStatus(source.getStatus());

        dispenserRepository.save(target);
    }

    public void delete(Dispenser dispenser) {
        dispenserRepository.delete(dispenser);
    }
}
