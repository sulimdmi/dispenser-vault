package com.sarsx.dispenservault.services;

import com.sarsx.dispenservault.models.Dispenser;
import com.sarsx.dispenservault.models.Product;
import com.sarsx.dispenservault.models.ProductManufacturer;
import com.sarsx.dispenservault.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public List<Product> findAllByDispenser(Dispenser dispenser) {
        return productRepository.findAllByDispenser(dispenser);
    }

    public Product getOne(Long id) {
        return productRepository.getOne(id);
    }

    public List<Product> findAllByProductManufacturer(ProductManufacturer productManufacturer) {
        return productRepository.findAllByProductManufacturer(productManufacturer);
    }

    public Product save(Product product) {
        return productRepository.save(product);
    }

    public void saveToDispenser(Product product, Dispenser dispenser) {
        product.setDispenser(dispenser);
        productRepository.save(product);
    }

    public void delete(Product product) {
        productRepository.delete(product);
    }

    public void update(Product target, Product source) {
        target.setBarcode(source.getBarcode());
        target.setProductManufacturer(source.getProductManufacturer());
        target.setName(source.getName());
        target.setType(source.getType());
        target.setQuantity(source.getQuantity());
        target.setDispenser(source.getDispenser());

        productRepository.save(target);
    }
}
