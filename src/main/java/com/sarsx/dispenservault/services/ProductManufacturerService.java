package com.sarsx.dispenservault.services;

import com.sarsx.dispenservault.models.ProductManufacturer;
import com.sarsx.dispenservault.repositories.ProductManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductManufacturerService {
    private ProductManufacturerRepository productManufacturerRepository;

    @Autowired
    public ProductManufacturerService(ProductManufacturerRepository productManufacturerRepository) {
        this.productManufacturerRepository = productManufacturerRepository;
    }

    public List<ProductManufacturer> findAll() {
        return productManufacturerRepository.findAll();
    }
}
