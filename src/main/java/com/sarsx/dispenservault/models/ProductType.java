package com.sarsx.dispenservault.models;

public enum ProductType {
    DRINK("Drink"),
    FOOD("Food");

    private final String name;

    ProductType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
