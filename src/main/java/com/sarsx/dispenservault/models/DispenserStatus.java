package com.sarsx.dispenservault.models;

public enum DispenserStatus {
    GOOD("Good"),
    WARNING("Warning"),
    ERROR("Error");

    private final String name;

    DispenserStatus(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
