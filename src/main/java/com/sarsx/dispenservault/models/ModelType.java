package com.sarsx.dispenservault.models;

public enum ModelType {
    HOT("Hot"),
    COLD("Cold");

    private final String name;

    ModelType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
