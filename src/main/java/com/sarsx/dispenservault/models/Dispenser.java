package com.sarsx.dispenservault.models;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "dispensers")
public class Dispenser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private DispenserStatus status = DispenserStatus.ERROR;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "models_id")
    private Model model;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "users_id")
    private User user;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "locations_id")
    private Location location;

    public Dispenser() {
    }

    public Dispenser checkStatus(List<Product> products) {
        Collections.sort(products);

        if (products.get(0).getQuantity() == 0) {
            setStatus(DispenserStatus.ERROR);
        }
        if (products.get(0).getQuantity() > 0 && products.get(0).getQuantity() <= getModel().getQueueSize() / 2) {
            setStatus(DispenserStatus.WARNING);
        }
        if (products.get(0).getQuantity() > getModel().getQueueSize() / 2) {
            setStatus(DispenserStatus.GOOD);
        }

        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DispenserStatus getStatus() {
        return status;
    }

    public void setStatus(DispenserStatus status) {
        this.status = status;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Manufacturer getManufacturer() {
        return getModel().getManufacturer();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dispenser dispenser = (Dispenser) o;
        return Objects.equals(id, dispenser.id) &&
                status == dispenser.status &&
                Objects.equals(model, dispenser.model) &&
                Objects.equals(user, dispenser.user) &&
                Objects.equals(location, dispenser.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status, model, user, location);
    }

    @Override
    public String toString() {
        return status.toString();
    }
}
