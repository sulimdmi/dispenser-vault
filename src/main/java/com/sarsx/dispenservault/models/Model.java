package com.sarsx.dispenservault.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "models")
public class Model {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private ModelType type;
    private Integer width;
    private Integer height;
    private Integer capacity;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "manufacturers_id")
    private Manufacturer manufacturer;

    public Model() {
    }

    public Integer getQueueSize() {
        return capacity / width / height;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModelType getType() {
        return type;
    }

    public void setType(ModelType type) {
        this.type = type;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Model model = (Model) o;
        return Objects.equals(name, model.name) &&
                type == model.type &&
                Objects.equals(width, model.width) &&
                Objects.equals(height, model.height) &&
                Objects.equals(capacity, model.capacity) &&
                Objects.equals(manufacturer, model.manufacturer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, width, height, capacity, manufacturer);
    }

    @Override
    public String toString() {
        return name;
    }
}
