package com.sarsx.dispenservault.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "products")
public class Product implements Comparable<Product> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String barcode;
    private String name;
    private ProductType type;
    private Integer quantity;
    private Double price;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "dispensers_id")
    private Dispenser dispenser;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_manufacturers_id")
    private ProductManufacturer productManufacturer;

    public Product() {
    }

    @Override
    public int compareTo(Product o) {
        return quantity - o.quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Dispenser getDispenser() {
        return dispenser;
    }

    public void setDispenser(Dispenser dispenser) {
        this.dispenser = dispenser;
    }

    public ProductManufacturer getProductManufacturer() {
        return productManufacturer;
    }

    public void setProductManufacturer(ProductManufacturer productManufacturer) {
        this.productManufacturer = productManufacturer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(barcode, product.barcode) &&
                Objects.equals(name, product.name) &&
                type == product.type &&
                Objects.equals(quantity, product.quantity) &&
                Objects.equals(price, product.price) &&
                Objects.equals(dispenser, product.dispenser) &&
                Objects.equals(productManufacturer, product.productManufacturer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, barcode, name, type, quantity, price, dispenser, productManufacturer);
    }

    @Override
    public String toString() {
        return name;
    }
}
