package com.sarsx.dispenservault.repositories;

import com.sarsx.dispenservault.models.Dispenser;
import com.sarsx.dispenservault.models.Product;
import com.sarsx.dispenservault.models.ProductManufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByDispenser(Dispenser dispenser);
    List<Product> findAllByProductManufacturer(ProductManufacturer productManufacturer);
}
