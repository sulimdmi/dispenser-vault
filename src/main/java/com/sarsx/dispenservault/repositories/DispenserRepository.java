package com.sarsx.dispenservault.repositories;

import com.sarsx.dispenservault.models.Dispenser;
import com.sarsx.dispenservault.models.Model;
import com.sarsx.dispenservault.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DispenserRepository extends JpaRepository<Dispenser, Long> {
    List<Dispenser> findAllByUser(User user);
    Dispenser findByModelAndUser(Model model, User user);
    List<Dispenser> findAllByUserAndModel_Name(User user, String name);
}
