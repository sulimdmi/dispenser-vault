package com.sarsx.dispenservault.repositories;

import com.sarsx.dispenservault.models.Manufacturer;
import com.sarsx.dispenservault.models.Model;
import com.sarsx.dispenservault.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModelRepository extends JpaRepository<Model, Long> {
    List<Model> findAllByManufacturer(Manufacturer manufacturer);
}
