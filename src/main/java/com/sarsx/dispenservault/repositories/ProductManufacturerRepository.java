package com.sarsx.dispenservault.repositories;

import com.sarsx.dispenservault.models.ProductManufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductManufacturerRepository extends JpaRepository<ProductManufacturer, Long> {
}
